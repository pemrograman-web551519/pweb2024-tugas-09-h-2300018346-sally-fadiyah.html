<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <title>Detail Gaji Karyawan</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #eef2f7;
            color: #333;
            text-align: center;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .container {
            background-color: #fff;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            width: 60%;
            max-width: 600px;
        }
        h2 {
            color: #4a90e2;
            font-size: 24px;
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0;
        }
        th, td {
            padding: 12px 15px;
            border: 1px solid #ddd;
            text-align: left;
            font-size: 18px;
        }
        th {
            background: linear-gradient(90deg, #4a90e2, #50a3d9);
            color: white;
            border: none;
        }
        tr:nth-child(even) {
            background-color: #f9f9f9;
        }
        tr:hover {
            background-color: #f1f1f1;
        }
        td {
            transition: background-color 0.3s ease;
        }
        .highlight {
            background-color: #d4edda !important;
        }
    </style>
</head>
<body>
    <div class="container">
        <?php
            $gaji = 1000000;
            $pajak = 0.1;
            $thp = $gaji - ($gaji * $pajak);

            echo "<h2>Detail Gaji Karyawan</h2>";
            echo "<table>";
            echo "<tr><th>Jenis</th><th>Nominal</th></tr>";
            echo "<tr><td>Gaji sebelum pajak</td><td>Rp. " . number_format($gaji, 0, ',', '.') . "</td></tr>"; 
            echo "<tr><td>Gaji yang dibawa pulang</td><td>Rp. " . number_format($thp, 0, ',', '.') . "</td></tr>"; 
            echo "</table>";
        ?>
    </div>
    <script>
        const rows = document.querySelectorAll('tr');
        rows.forEach(row => {
            row.addEventListener('mouseover', () => {
                row.classList.add('highlight');
            });
            row.addEventListener('mouseout', () => {
                row.classList.remove('highlight');
            });
        });
    </script>
</body>
</html>
